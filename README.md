## How to Use

Clone the repository

```bash
git clone https://brayvasq@bitbucket.org/brayvasq/wsionic.git
```

Move to the project folder

```bash
cd wsionic/
```

### Web Service

| Python version | pip version | OS        |
| -------------- | ----------- | --------- |
| Python 2.7.14  | pip 9.0.3   | Fedora 27 |

Move to the directory of the web service

```bash
cd webservice/
```

Install dependencies

```bash
pip install -r requirements.txt
```

Run the server

```bash
python src/main.py
```

Open the url: http://localhost:5000/customers/api and then you see:

```json
{
  "customers": [
    {
      "city": "emsland", 
      "id": 1, 
      "lastname": "kuhn", 
      "name": "samuel", 
      "phone": "(640) 051-1346"
    }, 
    {
      "city": "salzland", 
      "id": 2, 
      "lastname": "kolb", 
      "name": "kai", 
      "phone": "(753) 723-5206"
    }, 
    {
      "city": "hammel", 
      "id": 3, 
      "lastname": "kopetersenlb", 
      "name": "anton", 
      "phone": "(820) 825-6403"
    }, 
    {
      "city": "tecumseh", 
      "id": 4, 
      "lastname": "ross", 
      "name": "xavier", 
      "phone": "(378) 202-6949"
    }, 
    {
      "city": "skerries", 
      "id": 5, 
      "lastname": "sales", 
      "name": "sofia", 
      "phone": "(027) 056-1575"
    }
  ]
}
```

### Ionic App

Move to the directory of the ionic app

```bash
cd mobileapp/
```

Install dependencies

```bash
npm install
```

Run the app

```bash
ionic serve --lab
```
##### Preview
![customers.png](https://bytebucket.org/brayvasq/wsionic/raw/258c2d183ce4793ff9c8c87f561173a721ca6c3f/mobileapp/src/assets/imgs/customers.png)
