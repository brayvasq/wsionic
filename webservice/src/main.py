#!/usr/bin/env python3
from flask import Flask,jsonify

app = Flask(__name__)

CUSTOMERS = [
    {
        'id' : 1,
        'name' : u'samuel',
        'lastname': u'kuhn',
        'city': u'emsland',
        'phone': u'(640) 051-1346'
    },
    {
        'id' : 2,
        'name' : u'kai',
        'lastname': u'kolb',
        'city': u'salzland',
        'phone': u'(753) 723-5206'
    },
    {
        'id' : 3,
        'name' : u'anton',
        'lastname': u'kopetersenlb',
        'city': u'hammel',
        'phone': u'(820) 825-6403'
    },
    {
        'id' : 4,
        'name' : u'xavier',
        'lastname': u'ross',
        'city': u'tecumseh',
        'phone': u'(378) 202-6949'
    },
    {
        'id' : 5,
        'name' : u'sofia',
        'lastname': u'sales',
        'city': u'skerries',
        'phone': u'(027) 056-1575'
    }
]

@app.after_request
def after_request(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  return response

@app.route('/customers/api',methods=['GET', 'OPTIONS'])
def get_customers():
    return jsonify({'customers':CUSTOMERS})

if __name__ == '__main__':
    app.run(debug=True)