import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the CustomerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CustomerProvider {

  constructor(public http: HttpClient) {
    console.log('Hello CustomerProvider Provider');
  }

  getCustomers(){
    return this.http.get('http://localhost:5000/customers/api');
  }

}
