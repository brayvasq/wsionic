import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CustomerProvider } from '../../providers/customer/customer'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  customers : any = [];

  constructor(public navCtrl: NavController,
              public customerService:CustomerProvider) {

  }

  ionViewDidLoad(){
    this.customerService.getCustomers().subscribe(
      (data) =>{
        this.customers = data['customers'];
      },
      (error)=>{
        console.error(error);
      }
    );
  }

}
